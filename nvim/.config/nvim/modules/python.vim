autocmd FileType python map <buffer> <leader>b :w<CR>:exec '!python3' shellescape(@%, 1)<CR>
autocmd FileType python map <buffer> <leader>fo :Format<CR>

