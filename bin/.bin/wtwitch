#!/bin/bash
#
# Description: Browse and watch Twitch without being tracked.
# Homepage: https://gitlab.com/krathalan/wtwitch
# Version 1.0.2
#
# Copyright (C) 2019 krathalan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -----------------------------------------
# -------------- Guidelines ---------------
# -----------------------------------------

# This script follows the Google Shell Style Guide: 
# https://google.github.io/styleguide/shell.xml

# This script uses shellcheck: https://www.shellcheck.net/
# There are no "shellcheck disable=SCXXXX" lines in this script.

# Many functions in this script spawn subshell processes
trap "clean_up && kill 0" SIGINT

# See https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
set -Eeuo pipefail

# -----------------------------------------
# ----------- Program variables -----------
# -----------------------------------------

# Script (self) information
readonly SCRIPT_NAME=$(basename "$0")
readonly PROJECT_URL="https://gitlab.com/krathalan/wtwitch"

# Colors
readonly GREEN=$(tput bold && tput setaf 2)
readonly GREY=$(tput setaf 8)
readonly LIGHTGREEN=$(tput setaf 10)
readonly NC=$(tput sgr0) # No color/turn off all tput attributes
readonly ORANGE=$(tput setaf 3)
readonly PURPLE=$(tput setaf 13)
readonly RED=$(tput bold && tput setaf 1)
readonly TURQOISE=$(tput setaf 6)

# Subscription check cache
readonly CACHE_EXPIRY_TIME="60" # In seconds
readonly CACHE_FILE_DIRECTORY="${HOME}/.cache/${SCRIPT_NAME}"
readonly CACHE_OFFLINE_TEXT_FILE="${CACHE_FILE_DIRECTORY}/.stream_offline.txt"
readonly CACHE_ONLINE_TEXT_FILE="${CACHE_FILE_DIRECTORY}/.stream_online.txt"

# Config
readonly CONFIG_FILE_DIRECTORY="${HOME}/.config/${SCRIPT_NAME}"
readonly CONFIG_FILE="${CONFIG_FILE_DIRECTORY}/config.json"

# Used to calculate output string lengths (see check_twitch_streams_helper() and list_streamers_of_game_helper())
readonly TITLE_CHARACTERS=18
readonly TERMINAL_WIDTH=$(tput cols)

# Used by check_twitch_streams() for asynchronous loading of user subscriptions
readonly LOAD_SUBS_FILE="/tmp/.wtwitch.tmp" 

# Miscellaneous
readonly CHECK_MARK="${GREEN}✔${NC}"
readonly TWITCH_API_URL="https://api.twitch.tv"
readonly TWITCH_API_KEY="cotxsalhlctv8z572f7fant4b0sc3u"

# -----------------------------------------
# ------------- User variables ------------
# -----------------------------------------

# Values are loaded from $CONFIG_FILE (usually ~/.config/wtwitch/config.json)
userPlayer=""
userQuality=""
userSubscriptions=()

# -----------------------------------------
# --------------- Functions ---------------
# -----------------------------------------

#######################################
# Changes the default player in $CONFIG_FILE that gets passed to streamlink.
# Globals:
#   Colors: ORANGE, NC
#   CHECK_MARK
#   CONFIG_FILE
# Arguments:
#   $1: player to set as default
# Returns:
#   none
#######################################
change_player()
{
  local -r workingPlayers=("gnome-mpv" "mpv" "mplayer" "vlc")
  local passedCheckMarker="false"

  # Convert user input to lowercase
  local -r playerName="${1,,}"

  # Check to make sure user has input player actually installed
  check_command "${playerName}"

  # Check against known working players
  for i in "${workingPlayers[@]}"; do
    if [[ "${playerName}" == "${i}" ]]; then
      passedCheckMarker="true"
    fi
  done

  if [[ "${passedCheckMarker}" != "true" ]]; then
    printf "\n %sWarning%s: player %s may not work properly with Streamlink.\n" "${ORANGE}" "${NC}" "${playerName}"
  fi

  write_setting ".player" "${playerName}"
}

#######################################
# Changes the default quality in $CONFIG_FILE that gets passed to streamlink.
# Globals:
#   CHECK_MARK
#   CONFIG_FILE
#   PROJECT_URL
# Arguments:
#   $1: quality to set as default
# Returns:
#   none
#######################################
change_quality()
{
  local -r acceptableQualities=("worst" "160p" "360p" "480p" "720p" "720p60" "1080p60" "best")
  local passedCheckMarkerArray=()

  # Convert user input to lowercase
  local -r specifiedQuality="${1,,}"

  # User may try and enter fallback qualities; code should account for this
  IFS=',' read -r -a qualityArray <<< "$specifiedQuality"

  # For each quality the user has specified
  for i in "${qualityArray[@]}"; do
    # Check to make sure it's an acceptable quality
    for j in "${acceptableQualities[@]}"; do
      if [[ "${i}" == "${j}" ]]; then
        passedCheckMarkerArray+=("true")
      fi
    done
  done

  # Make sure there are an equal number of "true" elements in passedCheckMarkers as there are qualities in specifiedQuality
  if [[ ${#qualityArray[@]} -eq ${#passedCheckMarkerArray[@]} ]]; then
    write_setting ".quality" "${specifiedQuality}"
  else
    exit_script_on_failure "Quality ${specifiedQuality} is not an acceptable quality. See ${PROJECT_URL}/#wtwitch-q-quality-change-qualityquality for more information."
  fi
}

#######################################
# Checks to see if a specified command is available and exits the script if the command is not available.
# Globals:
#   PROJECT_URL
# Arguments:
#   $1: command to test
# Returns:
#   none
#######################################
check_command()
{
  if [[ ! -x "$(command -v "$1")" ]]; then
    exit_script_on_failure "Package $1 not installed. More information: ${PROJECT_URL}/#dependencies"
  fi
}

#######################################
# Checks to see which subscribed Twitch streams are online and prints the user's settings.
# Globals:
#   CACHE_OFFLINE_TEXT_FILE
#   CACHE_ONLINE_TEXT_FILE
#   CONFIG_FILE
#   LOAD_SUBS_FILE
#   userPlayer
#   userQuality
#   userSubscriptions
# Arguments:
#   none
# Returns:
#   none
#######################################
check_twitch_streams()
{
  load_subs --async &
  load_config
  
  wait

  # Manually assign userSubscriptions due to asynchronous sub loading
  mapfile -t userSubscriptions < "${LOAD_SUBS_FILE}"

  printf "\n Streamers online:"

  # Check cache expiry time
  local dateDiff="0"
  local lastSubscriptionUpdate
  lastSubscriptionUpdate=$(jq -r ".lastSubscriptionUpdate" "${CONFIG_FILE}")

  if [[ "${lastSubscriptionUpdate}" != "null" ]]; then
    local -r currentDate=$(LANG=C date +%s -d "now")
    lastSubscriptionUpdate=$(LANG=C date +%s -d "${lastSubscriptionUpdate}")
    dateDiff=$(( currentDate - lastSubscriptionUpdate ))
  fi
  
  # If the last subsription update is null (e.g. the subscriptions have never been checked), OR
  # if the time between now and the last subscription update is greater than the cache expiry time, OR
  # if either of the cache files do NOT exist yet
  if  [[ "${lastSubscriptionUpdate}" == "null" ]] || [[ $dateDiff -gt $CACHE_EXPIRY_TIME ]] \
  || [[ ! -f "${CACHE_ONLINE_TEXT_FILE}" ]] || [[ ! -f "${CACHE_OFFLINE_TEXT_FILE}" ]]; then
    # Remove old cache files
    clean_up

    # Ensure that both CACHE_ONLINE_TEXT_FILE and CACHE_OFFLINE_TEXT_FILE get written to; sometimes all streamers are online or offline
    touch "${CACHE_ONLINE_TEXT_FILE}" && touch "${CACHE_OFFLINE_TEXT_FILE}"

    for stream in "${userSubscriptions[@]}"; do
      check_twitch_streams_helper "${stream}" &
    done

    wait

    # Update lastSubscriptionUpdate time
    write_setting ".lastSubscriptionUpdate" "$(LANG=C date -d "now")"
  fi
  
  printf "\n"

  sort "${CACHE_ONLINE_TEXT_FILE}" 2> /dev/null

  printf "\n Streamers offline:\n"

  sort "${CACHE_OFFLINE_TEXT_FILE}" 2> /dev/null

  printf "\n Settings:"
  printf "\n   Player set to %s" "${userPlayer}"
  printf "\n   Quality set to %s\n" "${userQuality}"
}

#######################################
# Used by check_twitch_streams to facilitate multi-process fetching and parsing of json files.
# Globals:
#   Colors: RED, NC, TURQOISE, ORANGE
#   CACHE_OFFLINE_TEXT_FILE
#   CACHE_ONLINE_TEXT_FILE
#   TERMINAL_WIDTH
#   TWITCH_API_KEY
#   TWITCH_API_URL
# Arguments:
#   none
# Returns:
#   none
#######################################
check_twitch_streams_helper()
{
  # Get current info of streamer
  local -r streamerJson=$(download_file "${TWITCH_API_URL}/kraken/streams/$1")

  local streamType
  streamType=$(jq -r ".stream.stream_type" <<< "${streamerJson}")

  # Check if stream is online 
  if [[ "${streamType}" != "null" ]]; then
    # Give a warning if the stream isn't live (e.g. it's a rerun)
    if [[ "${streamType}" == "live" ]]; then
      streamType=""
    else
      streamType=", ${RED}${streamType}${NC}"
    fi

    local streamName
    streamName=$(jq -r ".stream.channel.status" <<< "${streamerJson}")

    local streamGame
    streamGame="$(jq -r ".stream.game" <<< "${streamerJson}")"

    local maxTitleLength=$(( TERMINAL_WIDTH - TITLE_CHARACTERS - ${#streamGame} - ${#streamType} - ${#1} ))

    # Truncate stream name if necessary; some streamers have excessively long stream titles
    if [[ ${#streamName} -gt ${maxTitleLength} ]]; then
      streamName=$(printf "%s" "${streamName}" | cut -c 1-${maxTitleLength})
      streamName="${streamName}…"
    fi

    printf "   %s%s%s: %s %s(playing %s%s%s)%s\n" "${LIGHTGREEN}" "$1" "${NC}" "${streamName}" "${ORANGE}" "${streamGame}" "${streamType}" "${ORANGE}" "${NC}" >> "${CACHE_ONLINE_TEXT_FILE}"
  else
    printf "   %s%s%s\n" "${GREY}" "$1" "${NC}" >> "${CACHE_OFFLINE_TEXT_FILE}"
  fi
}

#######################################
# Cleans up temporary file(s).
# Globals:
#   CACHE_ONLINE_TEXT_FILE
#   CACHE_OFFLINE_TEXT_FILE
# Arguments:
#   none
# Returns:
#   none
#######################################
clean_up()
{
  # Clean up
  rm -f "${CACHE_ONLINE_TEXT_FILE}"
  rm -f "${CACHE_OFFLINE_TEXT_FILE}"
  rm -f "${LOAD_SUBS_FILE}"
}

#######################################
# Downloads a file quietly using wget and passes the TWITCH_API_KEY as a header to the server. Returns the downloaded file.
# Globals:
#   TWITCH_API_KEY
# Arguments:
#   $1: file to download
# Returns:
#   downloaded file
#######################################
download_file()
{
  wget --quiet --output-document=- --header="Client-ID: ${TWITCH_API_KEY}" "$1"
}

#######################################
# Prints passed error message before premature exit. Prints everything to >&2 (STDERR).
# Globals:
#   Colors: RED, NC
#   SCRIPT_NAME
# Arguments:
#   $1: error message to print
# Returns:
#   none
#######################################
exit_script_on_failure() 
{
  if [[ $# -gt 0 ]]; then
    printf "\n %sError%s: %s\n" "${RED}" "${NC}" "$1" >&2
    printf "\n Exiting %s Bash script.\n" "${SCRIPT_NAME}" >&2
  fi
  
  clean_up
  printf "%s" "${NC}"
  exit 1
}

#######################################
# Lists the top streamers for a specified game.
# Globals:
#   TWITCH_API_KEY
#   TWITCH_API_URL
# Arguments:
#   $1: game to list top streamers of
# Returns:
#   none
#######################################
list_streamers_of_game()
{
  # Replace spaces in game name with %20 for URL
  local -r gameName=${1// /%20}

  # Get game_id
  local twitchJson
  twitchJson=$(download_file "${TWITCH_API_URL}/helix/games?name=${gameName}")
  
  local -r gameID=$(jq -r ".data[0].id" <<< "${twitchJson}")

  # Get streamers
  twitchJson=$(download_file "${TWITCH_API_URL}/helix/streams?game_id=${gameID}")

  printf "\n %s streamers:\n" "$1"

  local whileCounter=0

  local streamerName=""
  streamerName=$(jq -r ".data[${whileCounter}].user_name" <<< "${twitchJson}")

  while [[ "${streamerName}" != "null" ]]; do
    list_streamers_of_game_helper "${whileCounter}" "${twitchJson}" "${streamerName}" &
    whileCounter=$(( whileCounter + 1 ))
    streamerName=$(jq -r ".data[${whileCounter}].user_name" <<< "${twitchJson}")
  done

  wait
}

#######################################
# Used by list_streamers_of_game to facilitate multi-process fetching and parsing of json files.
# Globals:
#   Colors: TURQOISE, NC, ORANGE
#   TERMINAL_WIDTH
# Arguments:
#   $1: index of streamer to print info of
#   $2: json file containing streamer data
#   $3: name of streamer
# Returns:
#   none
#######################################
list_streamers_of_game_helper()
{
  local index="$1"
  local twitchJson="$2"
  local streamerName="$3"

  local streamTitle=""
  streamTitle=$(jq -r ".data[${index}].title" <<< "${twitchJson}")

  local viewers=""
  viewers=$(jq -r ".data[${index}].viewer_count" <<< "${twitchJson}")

 #local maxTitleLength=$(( TERMINAL_WIDTH - TITLE_CHARACTERS - ${#streamGame} - ${#streamType} - ${#1} ))
  local maxTitleLength=$(( TERMINAL_WIDTH - TITLE_CHARACTERS - ${#streamerName} - ${#viewers} ))

  # Truncate title if necessary
  if [[ ${#streamTitle} -gt ${maxTitleLength} ]]; then
    streamTitle=$(printf "%s" "${streamTitle}" | cut -c 1-${maxTitleLength})
    streamTitle="${streamTitle}…"
  fi

  printf "   %s%s%s: %s %s(%s viewers)%s\n" "${TURQOISE}" "${streamerName}" "${NC}" "${streamTitle}" "${ORANGE}" "${viewers}" "${NC}"
}

#######################################
# Lists the top games on Twitch.
# Globals:
#   Colors: TURQOISE, NC
#   TWITCH_API_KEY
#   TWITCH_API_URL
# Arguments:
#   none
# Returns:
#   none
#######################################
list_top_games()
{
  printf "\n Top games:\n"
  
  local -r twitchJson=$(download_file "${TWITCH_API_URL}/helix/games/top")

  local whileCounter=0

  local gameName=""
  gameName=$(jq -r ".data[${whileCounter}].name" <<< "${twitchJson}")

  while [[ "${gameName}" != "null" ]]; do
    printf "   %s. %s%s%s\n" $(( whileCounter + 1 )) "${PURPLE}" "${gameName}" "${NC}"

    # Refresh data
    whileCounter=$(( whileCounter + 1 ))
    gameName=$(jq -r ".data[${whileCounter}].name" <<< "${twitchJson}")
  done
}

#######################################
# Loads user player and quality settings from $CONFIG_FILE (usually ~/.config/wtwitch/config.json).
# Globals:
#   CONFIG_FILE
#   CONFIG_FILE_DIRECTORY
#   userPlayer
#   userQuality
# Arguments:
#   none
# Returns:
#   none
#######################################
load_config()
{
  # Create config file if it doesnt exist
  if [[ ! -f "${CONFIG_FILE}" ]]; then
    if [[ ! -d "${CONFIG_FILE_DIRECTORY}" ]]; then
      mkdir "${CONFIG_FILE_DIRECTORY}"
    fi
    printf "{\"player\": \"mpv\",\"quality\": \"best\",\"subscriptions\": []}" > "${CONFIG_FILE}"
  fi

  # Create cache folder if it doesn't exist
  if [[ ! -d "${CACHE_FILE_DIRECTORY}" ]]; then
    mkdir -p "${CACHE_FILE_DIRECTORY}"
  fi

  # Load settings
  userPlayer=$(jq -r ".player" "${CONFIG_FILE}")
  userQuality=$(jq -r ".quality" "${CONFIG_FILE}")
}

#######################################
# Loads user subscriptions from $CONFIG_FILE (usually ~/.config/wtwitch/config.json).
# Globals:
#   CONFIG_FILE
#   LOAD_SUBS_FILE
#   userSubscriptions
# Arguments:
#   $1: flag for --async to map userSubs manually after load_subs &
# Returns:
#   none
#######################################
load_subs()
{
  # Erase possible existing temp file
  rm -f "${LOAD_SUBS_FILE}"
  touch "${LOAD_SUBS_FILE}"

  local whileCounter=0
  local -r numberOfSubs=$(jq ".subscriptions | length" "${CONFIG_FILE}")

  while [[ ${whileCounter} -lt ${numberOfSubs} ]]; do
    load_subs_helper "${whileCounter}" &
    whileCounter=$(( whileCounter + 1 ))
  done  

  wait

  if [[ $# -gt 0 ]]; then
    if [[ "$1" != "--async" ]]; then
      mapfile -t userSubscriptions < "${LOAD_SUBS_FILE}"
    fi
  fi
}

#######################################
# Used by load_subs to facilitate multi-process parsing of json files.
# Globals:
#   CONFIG_FILE
#   LOAD_SUBS_FILE
# Arguments:
#   $1: index of streamer to get name of
# Returns:
#   none
#######################################
load_subs_helper()
{
  printf "%s\n" "$(jq -r ".subscriptions[$1].streamer" "${CONFIG_FILE}")" >> "${LOAD_SUBS_FILE}"
}

#######################################
# Prints information about how to use this script.
# Globals:
#   Colors: GREEN, NC
#   PROJECT_URL
#   SCRIPT_NAME
# Arguments:
#   none
# Returns:
#   none
#######################################
print_help()
{
  man wtwitch
}

#######################################
# Opens a stream with Streamlink.
# Globals:
#   TWITCH_API_KEY
#   TWITCH_API_URL
#   userPlayer
#   userQuality
# Arguments:
#   $1: name of streamer to watch
# Returns:
#   none
#######################################
stream()
{
  load_config
  
  # Use VLC as backup player if mpv is not installed
  if [[ ! -x "$(command -v "${userPlayer}")" ]]; then
    if [[ -x "$(command -v "vlc")" ]]; then
      userPlayer="vlc"
    else
      notify-send -i "gnome-twitch" "Wtwitch: streaming $1 failed" "Media player ${userPlayer} not found."
      exit_script_on_failure
    fi
  fi

  # Check to make sure streamer is online 
  # Get current info of streamer
  local -r streamerJson=$(download_file "${TWITCH_API_URL}/kraken/streams/$1")

  if [[ "$(jq -r ".stream.stream_type" <<< "${streamerJson}")" == "null" ]]; then
    notify-send -i "gnome-twitch" "Wtwitch: streaming $1 failed" "Streamer $1 is not online."
    exit_script_on_failure
  fi

  # Add cosmetic window title and enable hardware decoding if using mpv
  
  if [[ "${userPlayer}" == "mpv" ]]; then
  {
    if [[ $(pgrep wayland) || $(pgrep sway) ]]; then
    {
      {
        # Try
        streamlink -p "${userPlayer} --hwdec=auto --gpu-context=wayland" --title "Watching $1 on Twitch (\\\${{video-params/h}}p \\\${{estimated-vf-fps}}fps)" --twitch-disable-ads "https://www.twitch.tv/$1" "${userQuality}" 2> /dev/null
      } || {
        # Catch
        notify-send -i "gnome-twitch" "Wtwitch: streaming $1 failed" "It's possible that $1's stream does not support quality ${userQuality}, or that the streamer $1 does not exist."
        exit_script_on_failure
      }
    }
    else
    {
      {
        # Try
        streamlink -p "${userPlayer} --hwdec=auto" --title "Watching $1 on Twitch (\\\${{video-params/h}}p \\\${{estimated-vf-fps}}fps)" --twitch-disable-ads "https://www.twitch.tv/$1" "${userQuality}" 2> /dev/null
      } || {
        # Catch
        notify-send -i "gnome-twitch" "Wtwitch: streaming $1 failed" "It's possible that $1's stream does not support quality ${userQuality}, or that the streamer $1 does not exist."
        exit_script_on_failure
      }
    }
    fi
  }
  else
  {
    {
      # Try
      streamlink -p "${userPlayer}" --twitch-disable-ads "https://www.twitch.tv/$1" "${userQuality}" 2> /dev/null
    } || {
      # Catch
      notify-send -i "gnome-twitch" "Wtwitch: streaming $1 failed" "It's possible that $1's stream does not support quality ${userQuality}, or that the streamer $1 does not exist."
      exit_script_on_failure
    }
  }
  fi
}

#######################################
# Subscribes to a specific streamer.
# Globals:
#   CHECK_MARK
#   CONFIG_FILE
#   TWITCH_API_KEY
#   TWITCH_API_URL
# Arguments:
#   $1: streamer to subscribe to
# Returns:
#   none
#######################################
subscribe()
{
  load_subs --no-async

  # Convert to lowercase
  local -r streamerToSubscribeTo="${1,,}"

  # Check to make sure user isn't already subscribed
  if [[ $(jq ".subscriptions | length" "${CONFIG_FILE}") -gt 0 ]]; then
    local whileCounter=0

    local currentSubscription
    currentSubscription=$(jq -r ".subscriptions[${whileCounter}].streamer" "${CONFIG_FILE}")

    while [[ "${currentSubscription}" != "null" ]] && [[ "${currentSubscription}" != "${streamerToSubscribeTo}" ]]; do
      # Refresh data
      whileCounter=$(( whileCounter + 1))
      currentSubscription=$(jq -r ".subscriptions[$whileCounter].streamer" "${CONFIG_FILE}")
    done

    if [[ "${currentSubscription}" == "${streamerToSubscribeTo}" ]]; then
      exit_script_on_failure "You are already subscribed to ${streamerToSubscribeTo}."
    fi
  fi

  # Check to make sure streamer exists before subscribing
  local -r streamerJson=$(download_file "${TWITCH_API_URL}/kraken/channels/${streamerToSubscribeTo}")

  if [[ "$(jq -r ".status" <<< "${streamerJson}")" == "404" ]]; then
    exit_script_on_failure "Streamer ${streamerToSubscribeTo} does not exist."
  fi

  # Add subscription (can't use write_setting here due to unique jq syntax)
  local -r tempJson=$(jq ".subscriptions[.subscriptions | length] |= . + {\"streamer\":\"$streamerToSubscribeTo\"}" "${CONFIG_FILE}")
  printf "%s" "${tempJson}" > "${CONFIG_FILE}"

  # Invalidate cache
  write_setting ".lastSubscriptionUpdate" "null"

  printf "\n %s Successfully subscribed to %s.\n" "${CHECK_MARK}" "${streamerToSubscribeTo}"
}

#######################################
# Unsubscribes from a specific streamer.
# Globals:
#   CONFIG_FILE
#   CHECK_MARK
# Arguments:
#   $1: streamer to unsubscribe from
# Returns:
#   none
#######################################
unsubscribe()
{
  load_subs

  # Convert to lowercase
  local -r streamerToUnsubscribeFrom="${1,,}"

  # Find index of streamer in .subscriptions[]
  local whileCounter=0

  local currentSubscription
  currentSubscription=$(jq -r ".subscriptions[${whileCounter}].streamer" "${CONFIG_FILE}")

  while [[ "${currentSubscription}" != "$streamerToUnsubscribeFrom" ]] && [[ "${currentSubscription}" != "null" ]]; do
    # Refresh data
    whileCounter=$(( whileCounter + 1 ))
    currentSubscription=$(jq -r ".subscriptions[${whileCounter}].streamer" "${CONFIG_FILE}")
  done

  # Check to make sure user is subscribed before unsubscribing
  if [[ ! $whileCounter -lt $(jq ".subscriptions | length" "${CONFIG_FILE}") ]]; then
    exit_script_on_failure "You weren't subscribed to ${streamerToUnsubscribeFrom} in the first place."
  fi

  # Remove subscription
  local -r tempJson=$(jq "del(.subscriptions[${whileCounter}])" "${CONFIG_FILE}")
  printf "%s" "${tempJson}" > "${CONFIG_FILE}"

  # Invalidate cache
  write_setting ".lastSubscriptionUpdate" "null"
  printf "\n %s Successfully unsubscribed from %s.\n" "${CHECK_MARK}" "${streamerToUnsubscribeFrom}"
}

#######################################
# Writes a setting value to the config file.
# Globals:
#   CONFIG_FILE
# Arguments:
#   $1: key
#   $2: value
# Returns:
#   none
#######################################
write_setting()
{
  local -r tempJson=$(jq "$1 = \"$2\"" "${CONFIG_FILE}")
  printf "%s" "${tempJson}" > "${CONFIG_FILE}"
}

# -----------------------------------------
# ---------------- Script -----------------
# -----------------------------------------

check_command "wget"
check_command "jq"

if [[ $# -eq 0 ]]; then
  print_help
  printf "%s" "${NC}" && exit 0
fi

# Parse flags. From:
# https://stackoverflow.com/questions/7069682/how-to-get-arguments-with-flags-in-bash/7069755#7069755
while [[ $# -gt 0 ]]; do
  case "$1" in
    -h|--help)
      print_help
      printf "%s" "${NC}" && exit 0
      break
      ;;
    -c|--check)
      check_twitch_streams
      printf "%s" "${NC}" && exit 0
      break
      ;;
    -g)
      shift
      if [[ $# -gt 0 ]]; then
        list_streamers_of_game "$1"
        printf "%s" "${NC}" && exit 0
      else
        exit_script_on_failure "No game specified."
      fi
      break
      ;;
    --get-streamers*)
      if [[ $# -gt 1 ]]; then
        shift
      fi
      if [[ $# -gt 0 ]]; then
        gameToCheck=$(printf "%s" "$1" | sed -e 's/^[^=]*=//g')
        list_streamers_of_game "${gameToCheck}"
      else
        exit_script_on_failure "No game specified."
      fi
      printf "%s" "${NC}" && exit 0
      break
      ;;
    -p)
      shift
      if [[ $# -gt 0 ]]; then
        change_player "$1"
        printf "%s" "${NC}" && exit 0
      else
        exit_script_on_failure "No media player specified."
      fi
      break
      ;;
    --change-player*)
      if [[ $# -gt 1 ]]; then
        shift
      fi
      if [[ $# -gt 0 ]]; then
        player=$(printf "%s" "$1" | sed -e 's/^[^=]*=//g')
        change_player "${player}"
      else
        exit_script_on_failure "No media player specified."
      fi
      printf "%s" "${NC}" && exit 0
      break
      ;;
    -q)
      shift
      if [[ $# -gt 0 ]]; then
        change_quality "$1"
        printf "%s" "${NC}" && exit 0
      else
        exit_script_on_failure "No quality specified."
      fi
      break
      ;;
    --change-quality*)
      if [[ $# -gt 1 ]]; then
        shift
      fi
      if [[ $# -gt 0 ]]; then
        quality=$(printf "%s" "$1" | sed -e 's/^[^=]*=//g')
        change_quality "${quality}"
      else
        exit_script_on_failure "No quality specified."
      fi
      printf "%s" "${NC}" && exit 0
      break
      ;;
    -s)
      shift
      if [[ $# -gt 0 ]]; then
        subscribe "$1"
        printf "%s" "${NC}" && exit 0
      else
        exit_script_on_failure "No streamer specified."
      fi
      break
      ;;
    --subscribe*)
      if [[ $# -gt 1 ]]; then
        shift
      fi
      if [[ $# -gt 0 ]]; then
        streamerToSubscribeTo=$(printf "%s" "$1" | sed -e 's/^[^=]*=//g')
        subscribe "${streamerToSubscribeTo}"
      else
        exit_script_on_failure "No streamer specified."
      fi
      printf "%s" "${NC}" && exit 0
      break
      ;;
    -t|--top-games)
      list_top_games
      printf "%s" "${NC}" && exit 0
      break
      ;;
    -u)
      shift
      if [[ $# -gt 0 ]]; then
        unsubscribe "$1"
        printf "%s" "${NC}" && exit 0
      else
        exit_script_on_failure "No streamer specified."
      fi
      break
      ;;
    --unsubscribe*)
      if [[ $# -gt 1 ]]; then
        shift
      fi
      if [[ $# -gt 0 ]]; then
        streamerToUnsubscribeFrom=$(printf "%s" "$1" | sed -e 's/^[^=]*=//g')
        unsubscribe "${streamerToUnsubscribeFrom}"
      else
        exit_script_on_failure "No streamer specified."
      fi
      printf "%s" "${NC}" && exit 0
      break
      ;;
    *)
      break
      ;;
  esac
done

check_command "streamlink"
stream "$1" &> /dev/null &
