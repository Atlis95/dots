;ELC   
;;; Compiled
;;; in Emacs version 26.2
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\210\300\303!\210\300\304!\210\300\305!\210\300\306!\210\300\307!\210\300\310!\210\300\311!\210\300\312!\207" [require dash ht s pfuture treemacs-core-utils treemacs-customization treemacs-workspaces treemacs-dom inline treemacs-macros] 2)
(defvar treemacs--dirs-to-collpase\.py "/home/stan/.emacs.d/elpa/treemacs-20190615.1013/treemacs-dirs-to-collapse.py")
(defvar treemacs--git-status\.py "/home/stan/.emacs.d/elpa/treemacs-20190615.1013/treemacs-git-status.py")
(defvar treemacs--single-file-git-status\.py "/home/stan/.emacs.d/elpa/treemacs-20190615.1013/treemacs-single-file-git-status.py")
#@103 Maximum size for `treemacs--git-cache'.
If it does reach that size it will be cut back to 30 entries.
(defvar treemacs--git-cache-max-size 60 (#$ . 1040))
#@479 Stores the results of previous git status calls for directories.
Its effective type is HashMap<FilePath, HashMap<FilePath, Char>>.

These cached results are used as a standin during immediate rendering when
`treemacs-git-mode' is set to be deferred, so as to minimize the effect of large
face changes, epsecially when a full project is refreshed.

Since this table is a global value that can effectively grow indefinitely its
value is limited by `treemacs--git-cache-max-size'.
(defvar treemacs--git-cache (make-hash-table :size treemacs--git-cache-max-size :test 'equal) (#$ . 1202))
#@121 Get the git face for the given STATUS.
Use DEFAULT as default match.

STATUS: String
DEFAULT: Face

(fn STATUS DEFAULT)
(defalias 'treemacs--git-status-face #[514 "\300\267\202 \301\207\302\207\303\207\304\207\305\207\306\207\207" [#s(hash-table size 6 test equal rehash-size 1.5 rehash-threshold 0.8125 purecopy t data ("M" 6 "U" 8 "?" 10 "!" 12 "A" 14 "R" 16)) treemacs-git-modified-face treemacs-git-conflict-face treemacs-git-untracked-face treemacs-git-ignored-face treemacs-git-added-face treemacs-git-renamed-face] 4 (#$ . 1794)])
(byte-code "\300\301\302\303#\300\301\304\305#\300\301\306\305#\300\207" [function-put treemacs--git-status-face compiler-macro treemacs--git-status-face--inliner pure t side-effect-free] 6)
#@36 

(fn INLINE--FORM STATUS DEFAULT)
(defalias 'treemacs--git-status-face--inliner #[771 "\3002m \301!\203 \211\202 \302\303!\301!\203 \211\202  \302\304!\305\306\307\310DD\311\307\312DD\313\307\314DD\315\307\316DD\317\307\320DD\321\307\322DD\323	D\257	=\203P \211\202W \324DC\"\266\203=\203c \211\202j \324DC\"\266\2030\207" [inline--just-use macroexp-copyable-p make-symbol "status" "default" pcase "M" quote treemacs-git-modified-face "U" treemacs-git-conflict-face "?" treemacs-git-untracked-face "!" treemacs-git-ignored-face "A" treemacs-git-added-face "R" treemacs-git-renamed-face _ macroexp-let*] 17 (#$ . 2531)])
#@185 Return the appropriate face for PATH based on GIT-INFO.
If there is no git entry for PATH return DEFAULT.

PATH: Filepath
GIT-INFO: Hashtable
DEFAULT: Face

(fn PATH GIT-INFO DEFAULT)
(defalias 'treemacs--get-node-face #[771 "\303\304	\n#+\211\305\267\202* \306\202+ \307\202+ \310\202+ \311\202+ \312\202+ \313\202+ \207" [default key table nil gethash #s(hash-table size 6 test equal rehash-size 1.5 rehash-threshold 0.8125 purecopy t data ("M" 18 "U" 22 "?" 26 "!" 30 "A" 34 "R" 38)) treemacs-git-modified-face treemacs-git-conflict-face treemacs-git-untracked-face treemacs-git-ignored-face treemacs-git-added-face treemacs-git-renamed-face] 7 (#$ . 3178)])
(byte-code "\300\301\302\303#\300\301\304\305#\300\301\306\305#\300\207" [function-put treemacs--get-node-face compiler-macro treemacs--get-node-face--inliner pure t side-effect-free] 6)
#@43 

(fn INLINE--FORM PATH GIT-INFO DEFAULT)
(defalias 'treemacs--get-node-face--inliner #[1028 "\3002p \301!\203 \211\202 \302\303!\301!\203 \211\202  \302\304!\301!\203+ \211\202. \302\305!\306\307EE=\203@ \211\202G \310DC\"\266\203=\203S \211\202Z \310DC\"\266\203=\203f \211\202m \310DC\"\266\2030\207" [inline--just-use macroexp-copyable-p make-symbol "path" "git-info" "default" treemacs--git-status-face ht-get macroexp-let*] 14 (#$ . 4040)])
#@128 Cuts `treemacs--git-cache' back down to size.
Specifically its size will be reduced to half of `treemacs--git-cache-max-size'.
(defalias 'treemacs--resize-git-cache #[0 "\3032# \304	!)\211\n\305\245ZC\306\307\310\311\312\313!\314\"\315\316%\"\266\2020\207" [treemacs--git-cache table treemacs--git-cache-max-size --cl-block-__body__-- hash-table-count 2 maphash make-byte-code 514 "	\304\n\"*\210\305\300\211\242S\240Y\205 \306\307\310\"\207" vconcat vector [treemacs--git-cache key table remhash 0 throw --cl-block-__body__-- :done] 5 "\n\n(fn KEY _)"] 9 (#$ . 4522)])
#@100 Dummy with PATH.
Real implementation will be `fset' based on `treemacs-git-mode' value.

(fn PATH)
(defalias 'treemacs--git-status-process-function #[257 "\300\207" [nil] 3 (#$ . 5109)])
#@126 Run `treemacs--git-status-process-function' on PATH, if allowed for PROJECT.
Remote projects are ignored.

(fn PATH PROJECT)
(defalias 'treemacs--git-status-process #[514 "\211\300H\301=\205 \302!\207" [3 local-readable treemacs--git-status-process-function] 4 (#$ . 5303)])
#@104 Dummy with FUTURE.
Real implementation will be `fset' based on `treemacs-git-mode' value.

(fn FUTURE)
(defalias 'treemacs--git-status-parse-function #[257 "\301\302\303\206	 \304\")\211\207" [test nil make-hash-table :test equal] 4 (#$ . 5587)])
#@85 Start an extended python-parsed git status process for files under PATH.

(fn PATH)
(defalias 'treemacs--git-status-process-extended #[257 "\306\307\310#\211\205[ \311\312!\211\205! \n\311\313\f#+\262\211\205* \211\314H\262\211\2054 \315\316\"\262\211\205> \317\320\"\262\321\322!BBBBBB\323\324\"*\266\203\207" [file-name-handler-alist default-directory treemacs-dom default key table vc-call-backend Git root nil expand-file-name gethash 3 -map treemacs-dom-node->key -filter stringp "-O" number-to-string apply pfuture-new treemacs-python-executable treemacs--git-status\.py treemacs-max-git-entries treemacs-git-command-pipe] 11 (#$ . 5842)])
#@244 Parse the git status derived from the output of GIT-FUTURE.
The real parsing and formatting is done by the python process. All that's really
left to do is pick up the cons list and put it in a hash table.

GIT-FUTURE: Pfuture

(fn GIT-FUTURE)
(defalias 'treemacs--parse-git-status-extended #[257 "\211\203\261 \303!\304\305!\205 \305!\306!\204o \307!\310\311\312#\313\314\"\315\316\317#\262\262\262G\314W\203H \204n \320\321\315\322\316\323#\324\325\"#\210\202n \204Y \320\321\315\322\316\323#\324\326\"#\210\327\204m \320\321\315\322\316\323#\324\325\"#\210)\210\330!\331U\205\254 \332!\333!\203\204 \211\202\252 \327\204\230 \320\321\315\322\316\323#\324\334\"#\210)\204\251 \320\321\315\322\316\323#\324\335!#\210\336\262\266\202\206\277 \336\337\340\n\206\272 \341\")\211\262\207" [treemacs--no-messages inhibit-message test pfuture-await-to-finish fboundp pfuture-stderr s-blank\? s-trim s-replace "\n" ", " s-truncate 80 propertize face error message "%s %s" "[Treemacs]" font-lock-keyword-face format "treemacs-git-status.py wrote to stderr: %s" "treemacs-git-status.py wrote to stderr (see full output in *Messages*): %s" t process-exit-status 0 read hash-table-p "treemacs-git-status.py output: %s" "treemacs-git-status.py did not output a valid hash table. See full output in *Messages*." nil make-hash-table :test equal] 10 (#$ . 6525)])
#@68 Start a simple git status process for files under PATH.

(fn PATH)
(defalias 'treemacs--git-status-process-simple #[257 "\302!\303	B\304\305\306\307\310\311\312&\313\300#\210*\211\207" [default-directory process-environment f-canonical "GIT_OPTIONAL_LOCKS=0" pfuture-new "git" "status" "--porcelain" "--ignored" "-z" "." process-put] 8 (#$ . 7911)])
#@68 Parse the output of GIT-FUTURE into a hash table.

(fn GIT-FUTURE)
(defalias 'treemacs--parse-git-status-simple #[257 "\303\304\305\306\307$\203\205 \310!\210\311!\312U\203\205 \313\314\"\262\315!\204\204 \316\317\320\313\321\"#\322\323\324\325\312\326O\"\"\211G\312\211W\203\202 \2118\211@A@\211GSH\327=\204y \312H\330=\203` T\262\202y \331\332!\"\332!\312\333O\334	\n#\210+\266\211T\262\2029 \266\210\211\207" [value key table make-hash-table :test equal :size 300 pfuture-await-to-finish process-exit-status 0 process-get result s-blank\? vc-call-backend Git root default-directory mapcar #[257 "\300\301\302!\303#\207" [s-split-up-to " " s-trim 1] 5 "\n\n(fn IT)"] s-split " " -1 47 82 f-join s-trim-left 1 puthash] 15 (#$ . 8273)])
#@400 Apply the git fontification for direct children of PARENT-BTN.
GIT-FUTURE is parsed the same way as in `treemacs--create-branch'. Additionally
since this function is run on an idle timer the BUFFER to work on must be passed
as well since the user may since select a different buffer, window or frame.

PARENT-BTN: Button
GIT-FUTURE: Pfuture|HashMap
BUFFER: Buffer

(fn PARENT-BTN GIT-FUTURE BUFFER)
(defalias 'treemacs--apply-deferred-git-state #[771 "\306!\205\377 \205\377 r\211q\210\307	!)\nV\203 \310\311\312\313#\210\314\315\"\312\316	\f#+\205\374 \314\317\"\320>\205\374 \314\321\"T\203b \322\323\"\211\203P \211\202] \324!\325\323#\210\211\262\262\202r \312)\326\327)\206m \330\")\211\262*\331*	#\210+\312+\332!\211\262\205\371 \314\321\"Y\205\371 \314\315\"\314\321\"U\203\365 \314\333\"\204\365 \314\334\"\312\316	\f#+\211\335\267\202\331 \336\202\332 \337\202\332 \340\202\332 \341\202\332 \342\202\332 \343\202\332 \262\262\344\345T\346\"\206\350 e\347\346\"\206\360 d\350$\266\210\202\205 )\266\203\262)\207" [treemacs--git-cache table treemacs--git-cache-max-size treemacs-dom default key buffer-live-p hash-table-count run-with-idle-timer 2 nil treemacs--resize-git-cache get-text-property :path gethash :state (dir-node-open root-node-open) :depth process-get git-table treemacs--git-status-parse-function process-put make-hash-table :test equal puthash next-button :no-git :default-face #s(hash-table size 6 test equal rehash-size 1.5 rehash-threshold 0.8125 purecopy t data ("M" 193 "U" 197 "?" 201 "!" 205 "A" 209 "R" 213)) treemacs-git-modified-face treemacs-git-conflict-face treemacs-git-untracked-face treemacs-git-ignored-face treemacs-git-added-face treemacs-git-renamed-face put-text-property previous-single-property-change button next-single-property-change face test value buffer-read-only] 14 (#$ . 9048)])
#@160 Update the FILE node's git state, wrapped in `treemacs-save-position'.
Internally calls `treemacs-do-update-single-file-git-state'.

FILE: Filepath

(fn FILE)
(defalias 'treemacs-update-single-file-git-state #[257 "\304\305\306\307\310\311!\312\"\313$\216\314\315 \316 \317\304$\211\205 \320\321\"\262\322 \211\205Z \211\205Z \323\324\"\325\326\317\"\2069 d\321\"\211\203X \323\324\"W\203X \325\326\317\"\206Q d\321\"\262\202; \262\262\211\205d \327\330\"\262\211\205\247 \323\324\"\331\332T\317\"\206y e!\211\203\227 \323\324\"W\203\227 \331\332T\317\"\206\221 e!\262\202z \211\205\245 \323\324\"U\205\245 \211\266\202\262\211\205\261 \327\330\"\262\211\205\274 \323\330\"\262\211\205\307 \323\333\"\262\211\205\321 \334!\262\211\2057\323\330\"\211\203\345 \211\2025\335\332T\317\"\206\357 e\326\317\"\206\367 d\"\323\336\"\304\203(\323\330\"\204(\335\332T\317\"\206e\326\317\"\206d\"B\262\323\336\"\262\202\375 \323\330\"B\262B\266\203\262\262\337 \211\205`\340!r\305\306\341\310\311!\342\"\313$\216\343@\344\"\210\345\346\347 \316 \"]*\262\350\f!\210\351>\203\203\352\304\"\211\203\353 \210\354 \210\355\356 `\"\210\210\202`\357>\2034\360!\203\267	\204\237\361\n\362\363!!\"\204\267\352\304\"\211\203\263\353 \210\354 \210\355\356 `\"\210\210\202`\304\364\262\203\340\211	!\203\340\352	\304\"\211\203\334\353 \210\354 \210\355\356 `\"\210\210\2020\203\211!\203\352\304\"\211\203\353 \210\354 \210\355\356 `\"\210\210\2020\211;\203:\211\365\230\262\203\202\212\366!\211G\345V\2034\211GSH\367=\2034\211\306\370O\2025\211\262\202\212\211@\371=\203OG\313V\205\212\372!\202\212\211@;\203hG\313V\203c\372!\202\212@\202\212\373@!>\203\206G\313V\203\372!\202\212@\313H\202\212\374\375\"\262!\204\211\211;\203\306\211\365\230\262\203\245\202\366!\211G\345V\203\300\211GSH\367=\203\300\211\306\370O\202\301\211\262\202\211@\371=\203\332G\313V\205\372!\202\211@;\203\362G\313V\203\355\372!\202@\202\373@!>\203G\313V\203\372!\202@\313H\202\374\375\"\262\262\202\214\352\304\"\211\203.\353 \210\354 \210\355\356 `\"\210\266\210\202`\376>\203A\377!\210\202`\204Lb\210\202`\201@ 1\\\201A !0\202_\304\262\210\353 \210\323`\201B \"\203t\326`\201B \"b\210\211\205\231\340!r\305\306\341\310\311!\201C \"\313$\216\343@\344\"\210\201D S!*\262\266\212)\207" [treemacs--ready-to-follow treemacs-show-hidden-files treemacs-dotfiles-regex cl-struct-treemacs-project-tags nil make-byte-code 0 "\300\211\207" vconcat vector [treemacs--ready-to-follow] 2 text-property-not-all point-at-bol point-at-eol button copy-marker t point-marker get-text-property :depth next-button next-single-property-change button-get :path previous-button previous-single-property-change :state treemacs--nearest-path buffer-substring-no-properties :parent treemacs-get-local-window internal--before-with-selected-window "\301\300!\207" [internal--after-with-selected-window] select-window norecord 1 count-screen-lines window-start treemacs-do-update-single-file-git-state (root-node-closed root-node-open) treemacs-find-file-node treemacs--evade-image hl-line-highlight set-window-point get-buffer-window (file-node-closed file-node-open dir-node-closed dir-node-open) file-exists-p s-matches\? file-name-nondirectory directory-file-name #[257 "\302!\205 \206 \303	\304\305!!\"?\207" [treemacs-show-hidden-files treemacs-dotfiles-regex file-exists-p s-matches\? file-name-nondirectory directory-file-name] 6 "\n\n(fn IT)"] "/" file-name-directory 47 -1 :custom butlast type-of error "Path type did not match: %S" (tag-node tag-node-closed tag-node-open) treemacs--goto-tag-button-at (error) treemacs-goto-node invisible [internal--after-with-selected-window] recenter] 19 (#$ . 10949)])
#@325 Asynchronously update the given FILE node's git fontification.
Since an update to a single node can potentially also mean a change to the
states of all its parents they will likewise be updated by this function. If the
file's current and new git status are the same this function will do nothing.

FILE: Filepath

(fn FILE)
(defalias 'treemacs-do-update-single-file-git-state #[257 "p\211;\2035 \211\306\230\262\203 \202\202 \307!\211G\310V\203/ \211GSH\311=\203/ \211\312\313O\2020 \211\262\202\202 \211@\314=\203I G\315V\205\202 \316!\202\202 \211@;\203a G\315V\203\\ \316!\202\202 @\202\202 \317@!>\203~ G\315V\203w \316!\202\202 @\315H\202\202 \320\321\"\262	\322\323\f\n#+\211\310H\324\325\315H\322\203\252 B\262\315H\262\202\231 \262\"AB\211\205\300 \211\322\323\f\n#+\262\211\205\323 \211\322\323\f\n#+\262\206\331 \326<\327=BBBBB\330\331\332\333\334\335!\205\247r\fq\210\322>\336\337 !\211A\262\242\211A\262\242\340!\211\203M\341\267\2025\342\2026\343\2026\344\2026\345\2026\346\2026\347\2026\350\351\352T\353\"\206@e\354\353\"\206Hd\355$\266\266\211\211\205\242\211@\211@A\211\340!\211\203\231\356\267\202\201\342\202\202\343\202\202\344\202\202\345\202\202\346\202\202\347\202\202\357\351\352T\353\"\206\214e\354\353\"\206\224d\355$\266\266A\266\202\202P\262\262*\360\361?!\211\315=\203\266\322\202(\362@!\211GSH\363U\203\314\312O\202\315\262\262A\204\375\364\365\366\367\355\370#\371\372BGSBH\363U\203\366B\312O\202\370B\262##\210A?\205&\364\365\366\367\355\370#\371\373GSH\363U\203 \312O\202\"\262\"#\262\262&	\207" [cl-struct-treemacs-project-tags treemacs-dom default key table treemacs--git-cache "/" file-name-directory 1 47 0 -1 :custom 2 butlast type-of error "Path type did not match: %S" nil gethash -map treemacs-dom-node->key "0" "-O" pfuture-callback :directory :name "Treemacs Update Single File Process" :on-success buffer-live-p read pfuture-callback-output treemacs-find-visible-node #s(hash-table size 6 test equal rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (#1="M" 285 #2="U" 289 #3="?" 293 #4="!" 297 #5="A" 301 #6="R" 305)) treemacs-git-modified-face treemacs-git-conflict-face treemacs-git-untracked-face treemacs-git-ignored-face treemacs-git-added-face treemacs-git-renamed-face treemacs-git-unmodified-face put-text-property previous-single-property-change button next-single-property-change face #s(hash-table size 6 test equal rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (#1# 361 #2# 365 #3# 369 #4# 373 #5# 377 #6# 381)) treemacs-directory-face :on-error process-exit-status pfuture-output-from-buffer 10 message "%s %s" propertize "[Treemacs]" font-lock-keyword-face format "Update of node \"%s\" failed with status \"%s\" and result" "\"%s\"" treemacs-python-executable treemacs--single-file-git-status\.py buffer-read-only process pfuture-buffer treemacs--no-messages status] 28 (#$ . 14837)])
#@511 Start a new process to determine dirs to collpase under PATH.
Only starts the process if PROJECT is locally accessible (i.e. exists, and
is not remote.)
Output format is an elisp list of string lists that's read directly.
Every string list consists of the following elements:
 * The path that is being collapsed
 * The string to be appened to the collapsed path in the treemacs view
 * The single directories being collapsed, to be put under filewatch
   if `treemacs-filewatch-mode' is on.

(fn PATH PROJECT)
(defalias 'treemacs--collapsed-dirs-process #[514 "\305V\205( 	\205( \211\306H\307=\205( \310	\311\312!\f\203$ \313\202% \314&)\207" [treemacs-collapse-dirs treemacs-python-executable default-directory treemacs--dirs-to-collpase\.py treemacs-show-hidden-files 0 3 local-readable pfuture-new "-O" number-to-string "t" "x"] 9 (#$ . 17854)])
#@143 Parse the output of collpsed dirs FUTURE.
Splits the output on newlines, splits every line on // and swallows the first
newline.

(fn FUTURE)
(defalias 'treemacs--parse-collapsed-dirs #[257 "\211\205 \300!\301!\302U\205 \303!\262\207" [pfuture-await-to-finish process-exit-status 0 read] 4 (#$ . 18717)])
(byte-code "\300\301\302\303\304DD\305\306\307\310\311\312\313\314\315&\207" [custom-declare-variable treemacs-git-mode funcall function #[0 "\300\207" [nil] 1] "Non-nil if Treemacs-Git mode is enabled.\nSee the `treemacs-git-mode' command\nfor a description of this minor mode.\nSetting this variable directly does not take effect;\neither customize it (see the info node `Easy Customization')\nor call the function `treemacs-git-mode'." :set custom-set-minor-mode :initialize custom-initialize-default :group treemacs-git :type boolean] 12)
#@1372 Toggle `treemacs-git-mode'.
When enabled treemacs will check files' git status and highlight them
accordingly. This git integration is available in 3 variants: simple, extended
and deferred.

The simple variant will start a git status process whose output is parsed in
elisp. This version is simpler and slightly faster, but incomplete - it will
highlight only files, not directories.

The extended variant requires a non-trivial amount of parsing to be done, which
is achieved with python (specifically python3). It is slightly slower, but
complete - both files and directories will be highlighted according to their
git status.

The deferred variant is the same is extended, except the tasks of rendering
nodes and highlighting them are separated. The former happens immediately, the
latter after `treemacs-deferred-git-apply-delay' seconds of idle time. This may
be faster (if not in truth then at least in appereance) as the git process is
given a much greater amount of time to finish. The downside is that the effect
of nodes changing their colors may be somewhat jarring, though this effect is
largely mitigated due to the use of a caching layer.

All versions run asynchronously and are optimized for not doing more work than
is necessary, so their performance cost should, for the most part, be the
constant time needed to fork a subprocess.

(fn &optional ARG)
(defalias 'treemacs-git-mode #[256 "\301 \302\300\303=\203 \304\300!?\202 \305!\306V\"\210\2030 \307>\203) \310!\210\2023 \311\310!\210\2023 \312 \210\313\314\304\300!\203? \315\202@ \316\"\210\317\320!\203j \321\300!\210\301 \203X \211\301 \232\203j \322\323\324\304\300!\203e \325\202f \326#\266\210\327 \210\304\300!\207" [treemacs-git-mode current-message set-default toggle default-value prefix-numeric-value 0 (simple extended deferred) treemacs--setup-git-mode call-interactively treemacs--tear-down-git-mode run-hooks treemacs-git-mode-hook treemacs-git-mode-on-hook treemacs-git-mode-off-hook called-interactively-p any customize-mark-as-set "" message "Treemacs-Git mode %sabled%s" "en" "dis" force-mode-line-update] 7 (#$ . 19580) (byte-code "\206 \301C\207" [current-prefix-arg toggle] 1)])
(defvar treemacs-git-mode-hook nil)
(byte-code "\301\302N\204\f \303\301\302\304#\210\305\306\307\310\300!\205 \307\211%\207" [treemacs-git-mode-map treemacs-git-mode-hook variable-documentation put "Hook run after entering or leaving `treemacs-git-mode'.\nNo problems result if this variable is not bound.\n`add-hook' automatically binds it.  (This is true for all hook variables.)" add-minor-mode treemacs-git-mode nil boundp] 6)
#@115 Set up `treemacs-git-mode'.
Use either ARG as git integration value of read it interactively.

(fn &optional ARG)
(defalias 'treemacs--setup-git-mode #[256 "\211\211\301>\203 \302\303M\210\304\305M\207\306=\203 \302\307M\210\304\310M\207\302\311M\210\304\312M\207" [treemacs-git-mode (deferred extended) treemacs--git-status-process-function treemacs--git-status-process-extended treemacs--git-status-parse-function treemacs--parse-git-status-extended simple treemacs--git-status-process-simple treemacs--parse-git-status-simple ignore #[257 "\301\302\303\206	 \304\")\211\207" [test nil make-hash-table :test equal] 4 "\n\n(fn _)"]] 3 (#$ . 22206) (byte-code "\300\301\302\303\"\227!C\207" [intern completing-read "Git Integration: " ("Simple" "Extended" "Deferred")] 4)])
#@32 Tear down `treemacs-git-mode'.
(defalias 'treemacs--tear-down-git-mode #[0 "\300\301M\210\302\303M\207" [treemacs--git-status-process-function ignore treemacs--git-status-parse-function #[257 "\301\302\303\206	 \304\")\211\207" [test nil make-hash-table :test equal] 4 "\n\n(fn _)"]] 2 (#$ . 22992)])
#@171 Get the parsed git result of FUTURE.
Parse and set it if it hasn't been done yet. If FUTURE is nil an empty hash
table is returned.

FUTURE: Pfuture process

(fn FUTURE)
(defalias 'treemacs--get-or-parse-git-result #[257 "\211\203 \301\302\"\211\203 \211\202 \303!\304\302#\210\211\262\207\305\306\307\206& \310\")\211\207" [test process-get git-table treemacs--git-status-parse-function process-put nil make-hash-table :test equal] 7 (#$ . 23302)])
(byte-code "\300\301\302\303#\300\207" [function-put treemacs--get-or-parse-git-result compiler-macro treemacs--get-or-parse-git-result--inliner] 4)
#@28 

(fn INLINE--FORM FUTURE)
(defalias 'treemacs--get-or-parse-git-result--inliner #[514 "\3002F \211\301!\203 \211\202 \302\303!\304\305\306\307\310DE\311\312\313\314DDC\315\307\310D\313F\313FF\316CF=\203< \211\202C \317DC\"\266\2030\207" [inline--just-use macroexp-copyable-p make-symbol "future" if --if-let process-get quote git-table it let result treemacs--git-status-parse-function process-put ht macroexp-let*] 15 (#$ . 23917)])
(byte-code "\302\303!\204X \304\305!??\304\306!??B\211:\2035 \211@\211\307=\2034 A\211\310\267\2023 \311\312!\210\2023 \311\313!\210\210\210\210\211\203< \314\211\204V \315\316!\204V 	\204V \317\320\321\322\323\324#\325\326!#\210\266\327\330!\207" [treemacs-collapse-dirs treemacs--no-messages featurep treemacs executable-find "git" "python3" t #s(hash-table size 2 test eq rehash-size 1.5 rehash-threshold 0.8125 purecopy t data (t 40 _ 47)) treemacs-git-mode deferred simple 3 boundp treemacs-no-load-time-warnings message "%s %s" propertize "[Treemacs]" face font-lock-keyword-face format "Python3 not found, advanced git-mode and directory flattening features will be disabled." provide treemacs-async] 8)
